#!/bin/bash
echo "CREATE DATABASE IF NOT EXISTS $DB_NAME; USE $DB_NAME;" > /tmp/init.sql

/usr/bin/mysqld &
sleep 5
mysql < /tmp/init.sql

liquibase --url=jdbc:mysql://localhost/$DB_NAME \
	--username=$DB_USER \
	--password=$DB_PASSWORD \
	--changeLogFile=/tmp/changelog.xml migrate